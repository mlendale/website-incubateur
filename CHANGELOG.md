# CHANGELOG

## 1.1.0 (-)
### FEATURES
- Page "a propos"
- Page "services"
- Meilisearch plugin
### FIX
- Contact form
- Remove subtitle in offer page
- UI fix on link button

## 1.0.5 (2022-03-28)
### FEATURES
- Matomo setup
- LogRocket setup

## 1.0.4 (2022-03-25)
### FEATURES
- Pages "actualites"
- Created Vue components to embed dsfr

## 1.0.3 (2022-03-16)
### FEATURES
- Authenticated api connection

## 1.0.2 (2022-03-15)
### FEATURES
- Pages "investigation"

## 1.0.1 (2022-03-14)
### FIX
- Favicon updated

## 1.0.0 (2022-03-11)
### FEATURES
- Page home
- Page "mentions legales"
- Pages "recrutement"
- Page "contact"
- Page "donnees personnelles"
- Page "offre"
- Plan du site
- Connection with backend api
- General nuxt setup
